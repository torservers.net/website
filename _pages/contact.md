---
layout: page
title: Contact / Imprint
permalink: /contact/
redirect_from: /contact.html
---
For speedy and efficient processing of your request, please use the appropriate contact options.

## Tor related inquiries

For general Tor support, please use the resources provided by 'The Tor Project'. 

  - tor-talk (general community): [Mailing list](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-talk) or [Matrix](https://matrix.to/#/#tor:matrix.org)
  - tor-relays (support for Tor relay operators): [Mailing list](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays) or [Matrix](https://matrix.to/#/#tor-relays:matrix.org)

## Gerneral torservers.net related inquiries

[support@torservers.net](mailto:support@torservers.net) (PGP-Key on request)

## Press Inquiries

[press@torservers.net](mailto:press@torservers.net) (PGP-Key on request)

## Abuse handling

Please see the [abuse page](/abuse).

## Imprint

torservers.net is a project of it's [members](/members) without a legal identity.

Legally responsible for this website:   
Stefan Leibfarth   
Grüner Weg 32   
72766 Reutlingen   
Phone / Signal: +49 172 63 43 480

## Website privacy policy

We do not track our visitors, do not analyze them, do not set cookies and our web server does not store their IP. So that you don't have to believe us, do something so that we don't get any personal or personally identifiable data from you for analysis: Use the [Tor Browser](https://www.torproject.org/download/).