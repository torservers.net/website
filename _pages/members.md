---
layout: page
title: Members
permalink: /members/
rederect_from: /partners/
redirect_from: /partners.html
---
## Austria

<a href="https://appliedprivacy.net/"><img src="/assets/img/ffap.png" alt="Foundation for Applied Privacy" height="80" hspace="10" vspace="5"></a>
 
## Belgium

&nbsp;&nbsp;<a href="https://awp.is/"  style="padding: 22px 20px; border: 1.5px; border-style: solid; border-color: grey; display: inline-block;">Associated Whistleblowing Press</a>

## Chile

<a href="https://www.derechosdigitales.org"><img src="/assets/img/derechosdigitales.png" alt="Derechos Digitales" height="80" hspace="10" vspace="5"></a>
 
## Canada

<a href="https://coldhak.ca/"><img src="/assets/img/coldhak-logo.png" alt="Coldhak" height="80" hspace="10" vspace="5"></a>
<a href="https://koumbit.org"><img src="/assets/img/koumbit.png" alt="Koumbit" height="80" hspace="10" vspace="5"></a>

## Finland

<a href="https://effi.org/"><img src="/assets/img/effi.png" alt="Electronic Frontier Finland" height="80" hspace="10" vspace="5"></a>

## France

<a href="https://nos-oignons.net/"><img src="/assets/img/nos_oignons-logo.png" alt="Nos Oignons" height="80" hspace="10" vspace="5"></a>

## Germany

&nbsp;&nbsp;<a href="https://www.artikel5ev.de"  style="padding: 25px 18px; border: 1.5px; border-style: solid; border-color: grey; display: inline-block;">Artikel 5 e. V.</a>
&nbsp;&nbsp;<a href="https://artikel10.org"  style="padding: 25px 18px; border: 1.5px; border-style: solid; border-color: grey; display: inline-block;">Artikel 10 e. V.</a>
<a href="https://www.cccs.de"><img src="/assets/img/cccs-logo.png" alt="CCCS e.V." height="80" hspace="10" vspace="5"></a>
<a href="https://digitalcourage.de/support/tor"><img src="/assets/img/digitalcourage-logo.svg" alt="Digitalcourage e.V." height="40" hspace="10" vspace="5"></a>

## Iceland

<a href="https://icetor.is"><img src="/assets/img/icetor.png" alt="IceTor" height="80" hspace="10" vspace="5"></a>

## Italy

<a href="https://osservatorionessuno.org/"><img src="/assets/img/osservatorio_nessuno-logo.svg" alt="Osservatorio Nessuno" height="80" hspace="10" vspace="5"></a>

## Sweden

<a href="https://www.dfri.se/?lang=en"><img src="/assets/img/dfri-logo.png" alt="DFRI: Föreningen för Digitala Fri- och Rättigheter" height="80" hspace="10" vspace="5"></a>

## Switzerland

<a href="https://www.digitale-gesellschaft.ch"><img src="/assets/img/digiges-logo.svg" alt="Digitale Gesellschaft Schweiz" height="80" hspace="10" vspace="5"></a>

## Lebanon

<a href="https://www.cyber-arabs.com/"><img src="/assets/img/iwpr_logo.png" alt="Cyber Arabs (Institute for War &amp; Peace Reporting)" height="80" hspace="10" vspace="5"></a>

## Netherlands

&nbsp;&nbsp;<a href="https://www.hartvoorinternetvrijheid.nl"  style="padding: 22px 20px; border: 1.5px; border-style: solid; border-color: grey; display: inline-block;">Hart Voor Internetvrijheid</a>

## USA

<a href="https://www.accessnow.org/"><img src="/assets/img/access_now_logo.png" alt="Access Now" height="80" hspace="10" vspace="5"></a>
<a href="https://calyxinstitute.org/projects/digital-services/tor-anonymity"><img src="/assets/img/calyx-logo.png" alt="The Calyx Institute" height="80" hspace="10" vspace="5"></a>
