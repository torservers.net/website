---
layout: page
title: ISP Inquiry Templates
redirect_from: /wiki/hoster/inquiry/
permalink: /isp-inquiry-template/
---

    Subject: Tor Server Custom Build

    Hi,

    I am interested in your dedicated Gbit specials for hosting a Tor exit. 
    Can you give me a quote for a Dual Xeon E5620 2.4Ghz with 8GB RAM, 
    2x500GB RAID1 (or as small as you can get in terms of hard drives, 
    does not matter) and ARIN/RIPE IP reassignment for a small subnet 
    towards our organization?

    Thank you,

^

    Hi xxx,

    I am interested in your high volume servers for hosting Tor exit nodes.
    I've been referred to you by a happy customer.

    We are a registered non-profit from Germany working together with 
    security and cryptographic experts on censorship circumvention. Tor is 
    a research project that offers encrypted and safe tunnels for those 
    affected by Internet censorship or worried about their privacy.  

    Even though we only allow a number of well-known ports to exit from our 
    servers to not facilitate file sharing, Tor can unfortunately lead to a 
    number of (mostly unwarranted) abuse complaints. We have volunteers 
    doing 24/7 abuse handling and have successfully managed over 20 servers 
    in parallel in various jurisdictions.

    Are you fine with Tor hosting? I would also be very much interested in 
    a partly sponsored deal. I can put up banners for xxx on Torservers, 
    and we reach a lot of privacy aware people through various channels.

    We would be able to pay for 6 months up front, and need RIPE  
    reassignment of a /29 IP block (see https://apps.db.ripe.net/search/query.html?searchtext=ZWIEBELFREUNDE ).

    If you want to chat about it, you can find me on Jabber xxx.

    --
    John Doe
    
Follow up if positive response:

    Thank you for getting back to me so quickly.

    About Tor: We are a registered non-profit from Germany working 
    together with security and cryptography experts on censorship
    circumvention. Tor is a research project funded by the US Government
    (and others) that offers encrypted and safe tunnels to those affected by
    Internet censorship (China, Iran, Egypt, Burma etc). Among our partners
    are Human Rights Organizations and social movements from around the globe.

    Even though we only allow a number of well-known ports to exit from our
    servers to not facilitate file sharing, Tor can unfortunately lead to a
    number of (mostly unwarranted) abuse complaints. The Tor network and
    software blocks unprotected ports such as SMTP automatically, so Tor IPs
    rarely make it to blacklists.

    Even though we only allow a number of well-known ports to exit from our 
    servers to not facilitate file sharing, Tor can unfortunately lead to a 
    number of (mostly unwarranted) abuse complaints. We have volunteers doing 
    24/7 abuse handling and have successfully managed over 20 servers in 
    parallel in various jurisdictions.

    If you like to know more about the project, feel free to contact me on
    Jabber/XMPP bla@domain.example or visit our website at https://www.torservers.net/


## WHOIS Reassignent


Before you finally order make sure they will really give you proper WHOIS reassignment. Many promise to do so, but then don't want to change admin-c/tech-c/abuse records, which is crucial. Read RIPE/ARIN guidelines.

When an ISP in Europe, the Middle East or Rusia assigns IP-addresses to you as a customer, the ISP is supposed to register such an assignment in the RIPE database. When running a Tor exit-node you want to ask your ISP to make a customized registration, denoting the special use for these IP-addresses.

There are two reasons. First of all you want to make clear right away to anyone investigating traffic from these IP-addresses, that it's traffic you've been relaying only. Second, you want correspondence on abuse issues directed to yourself, instead of your ISP. To change the registration of the assignment, you have to contact your ISP.

## ARIN
[ARIN Reassignment Form](https://www.arin.net/resources/templates/reassign-simple.txt)

## RIPE

With RIPE, it works even better than with ARIN as most people respect the WHOIS entry there without going directly for the upstream record. In our experience, this happens a lot with ARIN. But - my guess is due to some stricter regulations by RIPE - less ISPs are willing to reassign RIPE IPs.
Some reports, like Shadowserver reports, get sent to the AS, so WHOIS does not help against those. With luck you can get your ISP to ignore or auto-forward them to you.

First you need to create the so-called PERSON and MNTNER objects in the RIPE-database. The second object is required to secure the first object. You can do this by <a href="https://apps.db.ripe.net/startup/" class="urlextern" title="https://apps.db.ripe.net/startup/" rel="ugc nofollow">filling out a form</a> on the website of RIPE, <a href="http://www.ripe.net/lir-services/training/e-learning/ripe-database/copy_of_ripe-database/create-your-first-person-and-maintainer-object-pair" class="urlextern" title="http://www.ripe.net/lir-services/training/e-learning/ripe-database/copy_of_ripe-database/create-your-first-person-and-maintainer-object-pair" rel="ugc nofollow">as explained here</a>. Both objects have a number of mandatory fields, like the address and phone fields for the PERSON object. Allthough RIPE does not check the validity of the entries, it's appreciated to enter valid information if possible.

Then you have to ask your ISP to change the registration of your IP-addresses. It is suggested your ISP adds “remarks” field to the INETNUM object denoting the IP-addresses within that block are used for routing Tor traffic. The other thing you should ask your provider is to set the fields “admin-c” and “abuse-mailbox” fields to your PERSON object. In the end it should state something similar to <a href="https://apps.db.ripe.net/search/query.html?searchtext=77.247.181.160&amp;searchSubmit=search#resultsAnchor" class="urlextern" title="https://apps.db.ripe.net/search/query.html?searchtext=77.247.181.160&amp;searchSubmit=search#resultsAnchor" rel="ugc nofollow">this example</a> or <a href="https://apps.db.ripe.net/search/query.html?searchtext=94.142.245.231&amp;searchSubmit=search#resultsAnchor" class="urlextern" title="https://apps.db.ripe.net/search/query.html?searchtext=94.142.245.231&amp;searchSubmit=search#resultsAnchor" rel="ugc nofollow">this example</a>. If you want to improve the changes of getting this right, prepare the form for your ISP to submit. These changes shouldn't take a lot of time for your ISP.

RIPE does not require <code>country</code> to be the location of your server, nor your own location. Your ISP nevertheless might want either your location or the servers location in there. I am not a fan of confusing GeoIP (and users), but you could specify any country. It is even possible to list more than one country, but be aware that it might take several months for GeoIP services to pick up changes (if they ever do) and that you won't be able to update the <code>inetnum</code> record yourself later.


### Example records

  - [mtner](https://apps.db.ripe.net/db-web-ui/query?bflag=false&dflag=false&rflag=true&searchtext=zwiebelfreunde&source=RIPE&types=mntner)
  - [person](https://apps.db.ripe.net/db-web-ui/query?bflag=false&dflag=false&rflag=true&searchtext=MB22990-RIPE&source=RIPE&types=person)
  - [inetnum](https://apps.db.ripe.net/db-web-ui/query?bflag=false&dflag=false&rflag=true&searchtext=77.247.181.160&source=RIPE&types=inetnum)

### Documentation
Additional RIPE Documentation for ISPs (it sometimes helps to pass that on in case they don't know how to do reassignments):

  - [http://www.ripe.net/lir-services/resource-management/faq/internet-resources#faq_22](http://www.ripe.net/lir-services/resource-management/faq/internet-resources#faq_22)
  - [http://www.ripe.net/ripe/docs/ripe-489](http://www.ripe.net/ripe/docs/ripe-489)
