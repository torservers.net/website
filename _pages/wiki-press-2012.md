---
layout: page
title: Press 2012
permalink: /wiki/press/2012/
---
<a href="/assets/pdf/Onions-Against-Dictators-Wau-Holland-Foundation-2012.pdf" class="urlextern">Wau Holland Foundation and Zwiebelfreunde e.V. Partner Against Internet Censorship</a> (PDF)

<h3 class="sectionedit1" id="wau_holland_foundation_and_zwiebelfreunde_ev_partner_against_internet_censorship">Wau Holland Foundation and Zwiebelfreunde e.V. Partner Against Internet Censorship</h3>
<div class="level3">

<p>
(Dresden, 12.03.2012) The non-profit organization Zwiebelfreunde e.V. has received a 12.000€ grant from Wau Holland Foundation to ensure its abilities to finance Tor anonymization relays.  “I am very grateful for the trust of Wau Holland Foundation and their decision to help our fight against Internet censorship.”, says founder Moritz Bartl.
</p>

<p>
Uncensored Internet access can overthrow dictators and aid the creation of free societies. Whether in Egypt, Libya or Tunisia, we have all seen the impressive impact of free access to information and unhindered communication.
</p>

</div>

<h3 class="sectionedit2" id="tor_anonymisation_project">Tor Anonymisation Project</h3>
<div class="level3">

<p>
With the free software Tor, Internet traffic is securely passed through relay stations to prevent censorship and surveillance (www.torproject.org). The bandwidth and computers used for relaying traffic are mostly provided by volunteers, many using their home computers. Every day more than 500,000 people use Tor, including human rights activists, government officials, police investigators, journalists, non-profits and private individuals. Torservers.net helps the network grow by making additional resources available to those in need of protecting their personal freedom and privacy online. These resources greatly improve the overall speed and availability of the Tor network.
</p>

</div>

<h3 class="sectionedit3" id="torserversnet_receives_12_000_grant_from_wau_holland_foundation">Torservers.net receives 12,000€ grant from Wau Holland Foundation</h3>
<div class="level3">

<p>
The Torservers.net project is the most prominent activity of the German non-profit Zwiebelfreunde eV. Thanks to numerous private donations by supporters and the work of its volunteers, it quickly grew into one of the largest operators of the Tor network. Partnering with well-known groups such as Access Now before, Torservers.net is proud to announce a new partnership with the renowned Wau Holland Foundation. This grant of 12,000 Euro secures the operation of its anti-censorship gateways for one year.
</p>

</div>

<h3 class="sectionedit4" id="zwiebelfreunde_ev">Zwiebelfreunde e.V.</h3>
<div class="level3">

<p>
The German non-profit association Zwiebelfreunde eV serves as a platform for different projects in the area of safe and anonymous communications. The organization organizes and participates in educational events about technological advances in the area and connects professionals to spread knowledge and experiences on these fields. 
</p>

<p>
“Zwiebelfreunde” is German for „Friends of the Onion“ as a reference to Onion Routing, the name of the concept Tor is based on to anonymize communication: Messages are passed through nodes that remove and add layers of encryption, like peeling the skin of an onion.
</p>
