---
layout: page
title: Tor with libevent2
permalink: /wiki/setup/libevent2/
---
## ⚠️ This page is outdated ⚠️

Benefits unsure; please let us know of your results!

<h2 class="sectionedit2" id="libevent2">libevent2</h2>
<div class="level2">

<p>
<a href="http://monkey.org/~provos/libevent2" class="urlextern" title="http://monkey.org/~provos/libevent2" rel="ugc nofollow">http://monkey.org/~provos/libevent2</a>
</p>
<pre class="code">make clean
./configure --prefix=/usr/local/libevent --with-pic
make
make install</pre>

</div>

<h2 class="sectionedit3" id="tor">Tor</h2>
<div class="level2">
<ul>
<li class="level1"><div class="li"> based on <a href="http://moblog.wiredwings.com/archives/20100427/Tor-on-Debian,-self-compiled-for-better-Performance.html" class="urlextern" title="http://moblog.wiredwings.com/archives/20100427/Tor-on-Debian,-self-compiled-for-better-Performance.html" rel="ugc nofollow">http://moblog.wiredwings.com/archives/20100427/Tor-on-Debian,-self-compiled-for-better-Performance.html</a></div>
</li>
<li class="level1"><div class="li"> add to debian/rules: <pre class="code">--with-libevent-dir=/usr/local/libevent \</pre>
