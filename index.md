---
layout: home
share-description: Global network of non-profits running Tor relays 
redirect_from: /about.html
---
{: style="text-align: center" }
<h1><nobr>torservers.net</nobr></h1>

{: style="text-align: center" }
**Global network of non-profits running Tor relays**

{: style="text-align: justify" }
*"Everyone has the right to freedom of opinion and expression; this right includes freedom to hold opinions without interference and to seek, receive and impart information and ideas through any media and regardless of frontiers."* (Art. 19, [Universal Declaration of Human Rights](https://www.un.org/en/about-us/universal-declaration-of-human-rights)) <br>

## What we do

  - Organising **relay operator meetups** to strengthen the community and **support each other**.
  - Point of [contact](/contact) for **press and politics**.
  - As a [RIPE member](https://www.ripe.net/membership/indices/data/de.zwf.html) we provide **private and stable IPs** to our partners.
  - Contact persons for **potential new relay operators**. [Contact us](/contact) if you think a about becoming or starting a relay operating organisation.
  - **Outreach work**, e.g. lectures, panel discussion, etc.
  - **Appling for funding money** and distribute it among our partners.
  - Helping to **find lawyers** and support for relay operators in trouble with the authorities.

## Latest blog posts

{% assign posts = paginator.posts | default: site.posts %}

<div class="posts-list">
   <ul>
  {% for post in posts limit:3 %}
    <li><a href="{{ post.url | absolute_url }}">{{ post.title | strip_html }}</a></li>
  {% endfor %}
  </ul>
</div>

## Contributions and Support Pledges

Torservers.net is being supported by many many individuals (thanks!) and has received institutional support from:

<center>
 <a href="https://www.wauland.de"><img src="assets/img/waulogo12.png" alt="Wau Holland Foundation" height="80" hspace="10" vspace="10"></a>
 <a href="http://digitaldefenders.org/" alt="Digital Defenders"><img src="assets/img/hivoslogo.jpg" height="80" hspace="10" vspace="10"></a>
 <a href="https://renewablefreedom.org/"><img src="assets/img/rff_logo.png" alt="Renewable Freedom Foundation" height="80" hspace="10" vspace="10"></a>
 <a href="https://accessnow.org/"><img src="assets/img/access_now_logo.png" alt="Access Now" height="60" hspace="10" vspace="10"></a>
 <a href="https://rsf.org/"><img src="assets/img/reporters_without_borders_logo.png" alt="Reporters Without Borders" height="63" hspace="10" vspace="10"></a>
 <a href="https://iwpr.net/"><img src="assets/img/iwpr_logo.png" alt="Institute for War &amp; Peace Reporting" height="60" hspace="10" vspace="10"></a>
</center>
