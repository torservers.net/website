---
layout: post
title: We have a new server in Australia!
author: Moritz
---
I've been contacted by networkpresence.com.au, a nice ISP in Australia.
They have donated a small server for running another Tor exit node with
1MB/s of traffic.

Thanks!
